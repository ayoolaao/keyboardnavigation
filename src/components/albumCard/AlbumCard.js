import React from 'react';
import './albumCard.css'

const AlbumCard = ({ title, index }) => {
  const colorClassNames = () => {
    const colors = [
      'pale-violet',
      'burly-wood',
      'light-steel-blue',
      'steel-blue',
      'light-steel-blue-alt',
      'pale-goldenrod',
      'powder-blue',
      'gray',
      'dark-sea-green',
      'dark-khaki',
      'dark-slate-blue',
      'dark-salmon',
      'dark-olive-green',
      'thistle',
      'pink',
      'purple',
      'pink-alt',
      'yellow-alt',
      'yellow',
      'pink-alt-2',
    ]

    return colors[Math.floor(Math.random() * colors.length)]
  }

  return (
    <div className={`card__wrapper ${colorClassNames()}`} tabIndex={0} data-index={index}>
      { title }
    </div>
  );
};

export default AlbumCard;
