import { useState, useEffect } from 'react';

const UseFetchAlbums = (url , options) => {
  const [ albums, setAlbums ] = useState(null);
  const [ error, setError ] = useState(null);
  const [ isLoading, setIsLoading ] = useState(false);

  useEffect(() => {
    const fetchAlbums = async () => {
      setIsLoading(true);

      try {
        const res = await fetch(url, options)
        const json = await res.json()

        setAlbums(json)
        setIsLoading(false)
      } catch (error) {
        setError(error)
      }
    };

    fetchAlbums()
  }, [])  //eslint-disable-line

  return { albums, isLoading, error }
};

export default UseFetchAlbums;
