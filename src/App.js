import React, { useEffect, useState } from 'react';
import './App.css';
import { AlbumCard } from './components';
import { useFetchAlbums } from './hooks'

const App = () => {
  const { albums, isLoading, error } = useFetchAlbums('https://jsonplaceholder.typicode.com/albums?_limit=8');
  const [ startCardIndex ] = useState(0)
  const cardsPerRow = 8

  const handleKeyBoardNavigation = e => {
    const albumsCount = albums.length - 1

    const { keyCode, target } = e
    const currentCardIndex = Number(target.getAttribute('data-index'))
    let nextCardIndex;

    switch (keyCode){
      case 37: //ArrowLeft
        nextCardIndex = currentCardIndex - 1
        if(nextCardIndex < 0) nextCardIndex = 0
        break;
      case 38: //ArrowUp
        nextCardIndex = currentCardIndex - cardsPerRow
        if(nextCardIndex < 0) nextCardIndex = currentCardIndex
        break;
      case 39: //ArrowRight
        nextCardIndex = currentCardIndex + 1
        if(nextCardIndex > albumsCount) nextCardIndex = currentCardIndex
        break;
      case 40: //ArrowDown
        nextCardIndex = currentCardIndex + cardsPerRow
        if(nextCardIndex > albumsCount) nextCardIndex = currentCardIndex
        break;
      default:
        return;
    }

    const currentCard = document.querySelector(`div[data-index="${currentCardIndex}"]`);
    const nextCard = document.querySelector(`div[data-index="${nextCardIndex}"]`);

    if(currentCard.classList.contains('active-card')) currentCard.classList.remove('active-card')

    nextCard.focus();
    nextCard.classList.add('active-card')
  }

  useEffect(() => {
    window.addEventListener('keydown', handleKeyBoardNavigation)
    if(albums) {
      const initialCard = document.querySelector(`div[data-index="${startCardIndex}"]`)
      initialCard.focus()
      initialCard.classList.add('active-card')
    }

    return () => { window.removeEventListener('keydown', handleKeyBoardNavigation) }
  })

  useEffect(() => {
    if (albums) {
      const cardsWrapper = document.querySelector('div.app__albums');
      console.log(cardsWrapper);
      cardsWrapper.style.gridTemplateColumns = `repeat(${cardsPerRow}, 1fr)`;
    }
  }, [albums])

  return (
    <div className="app">
      <div className='app__albums'>
        { isLoading && (<div>Loading..</div>) }
        { error && (<div>{error}</div>) }
        { albums && albums.map(({ id, title }, index) => (
          <AlbumCard key={id} title={title} index={index} />
        )) }
      </div>
    </div>
  );
}

export default App;
